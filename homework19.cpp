
#include <iostream>
#include <string>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "text" << '\n';
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof!" << '\n';
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow!" << '\n';
    }
};

class Sheep : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Baa!" << '\n';
    }
};

int main()
{
    Animal *array[3];
    array[0] = new Dog;
    array[1] = new Cat;
    array[2] = new Sheep;
    for (int i = 0; i < 3; i++)
    {
        array[i]->Voice();
    }

}
